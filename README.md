### Sandbox Details

This sandbox is intended for demo purposes only. This web application is built 
with AngularJS. Mocki is used in the backend to provide mock data for service
requests. The sandbox requires the following:
- npm
- @angular/cli

The sandbox works with angular's default packages. Other details regarding the 
application includes:
- CSS Library : Bootstrap
- JQuery used only for bootstrap JS to function

To test this application, install NodeJS.
NodeJS includes npm which is the package manager used for node applications.

Once Node and npm are installed, install the angular cli globally
```
npm i -g @angular/cli
```
This will install the latest angular cli and will allow you to run angular
commands from the terminal

Once the cli has been installed, clone the repository
```
git clone https://<username>@bitbucket.org/rpenero_blueprint/angular-sandbox.git
```
Open your repo from the terminal and install the node packages 
```
npm i
```
This will install all dependencies and dev dependencies required for the
application to properly function

#### File Structure

All files used during development are stored in the `src` folder.
The following are the folders that make up the application:
- Components : `/src/app/component`
- Services : `/src/app/service`
- Interface : /src/app/interface
