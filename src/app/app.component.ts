import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'angular-sandbox';
  showNav = false;

  constructor(private router: Router) {
  }

  /**
   * Used to detect navigation change and control navigation render
   */
  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.showNav = this.router.url !== '/';
      }
    });
  }
}
