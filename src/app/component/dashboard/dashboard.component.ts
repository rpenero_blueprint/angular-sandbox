import { Component, OnInit } from '@angular/core';
import {UserService} from '../../service/user.service';
import {User} from '../../interface/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  users: Array<User>;
  usersDataLength = 0;

  constructor(
    private userService : UserService
  ) { }

  async ngOnInit(): Promise<void> {
    this.users = await this.userService.getUsers();
    this.usersDataLength = this.users.length;
  }
}
