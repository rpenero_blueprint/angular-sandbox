import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  /**
   * interface for login
   */
  async login(username, password): Promise<void> {
    const isValidLogin = await this.userService.getUserbyCredentials(username, password);

    if (isValidLogin) {
      this.router.navigateByUrl('/dashboard');
    }
  }
}
