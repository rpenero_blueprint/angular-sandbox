import { Injectable } from '@angular/core';
import axios from 'axios';
import {User} from '../interface/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private BASE_URL = 'https://api.mocki.io/v1/a76e7a33';

  constructor() { }

  /**
   * Send post request to mock api
   */
  async getUserbyCredentials(username, password): Promise<boolean> {
    const user = await axios.post(this.BASE_URL + '/user/getUserByCredentials', {
      username,
      password
    });
    return user.data !== undefined;
  }


  /**
   * Retrieves all user data
   */
  async getUsers(): Promise<Array<User>> {
    const userResponse = await axios.get(this.BASE_URL + '/user/all');
    const users: Array<User> = userResponse.data.map((user) => {
      return {
        userId : user.userId.$oid,
        firstName : user.firstName,
        lastName : user.lastName,
        email : user.email
      };
    });
    return users;
  }
}
